---
title: Programming Resources
layout: my-default
description: Your Off the Cuff, Hot Stuff, Good Enuff Guide to Programming Resources
---

# Your Off the Cuff, Hot Stuff, Good Enuff Guide to Programming Resources

- [Resources](#resources)
  * [Articles](#articles)
  * [Books](#books)
  * [Collections of Resources](#collections-of-resources)
  * [Dive Into Some Source Code](#dive-into-some-source-code)
  * [Courses/Lectures](#courseslectures)
  * [Tutorials](#tutorials)
  * [Accessability](#accessability)
- [Overview & Motivations](#overview-%26-motivations)
  * [Why *Another* List of Programming Resources?!](#why-another-list-of-programming-resources%3F!)
  * [(Fuzzy) Requirements In Rough Order of Importance](#\(fuzzy\)-requirements-in-rough-order-of-importance)

## Resources

### Articles
- [Teach Yourself Programming in Ten Years][10-years]

[10-years]: https://norvig.com/21-days.html

### Books
- [Structure and Interpretation of Computer Programs (PDF)][sicp-pdf]
    - [Structure and Interpretation of Computer Programs (Other Formats)][sicp-ebooks]
- [MIT OpenCourseWare][ocw]

[sicp-pdf]: https://github.com/sarabander/sicp-pdf
[sicp-ebooks]: https://sicpebook.wordpress.com/ebook/https://sicpebook.wordpress.com/ebook/
[ocw]: https://github.com/sindresorhus/awesome

### Collections of Resources
- [Awesome list on Github](https://github.com/sindresorhus/awesome)
- [hackr.io](https://hackr.io/)
- [/r/learnprogramming online resources](https://old.reddit.com/r/learnprogramming/wiki/online)
- [/r/learnprogramming books](https://old.reddit.com/r/learnprogramming/wiki/books)

### Dive Into Some Source Code
- [Github explore](https://github.com/explore)
- [Gitlab explore](https://gitlab.com/explore/)
- [Codetriage](https://www.codetriage.com/)
- [Up for Grabs](https://up-for-grabs.net/)

### Courses/Lectures
- [MIT OpenCourseWare](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/)

### Tutorials
- [Free Code Campe](https://www.freecodecamp.org/)

### Accessability
- [a11y Project](https://a11yproject.com/resources/)

---

## Overview & Motivations

All listed resources will favor those that are free (as in free parking), in order to be as accessible as possible. There is an absolute *Universe* of information dedicated to programming and computer science. Don't feel compelled to read each and every textbook and tutorial out there. Your own path will only be directed by what you find interesting and worth spending your time on.

Programming is great, but it is not the end all, be all. There a million different pursuits out there, like cooking, painting, welding, tree climbing, etc.

One strength of computer coding in general is that is has a low cost of entry. Thanks to efforts like Free/Open Source software and Creative Commons, many of the same textbooks and resources available to professional programmers are avilable to everyone with an access to the internet. There are of course paid resources. But (at least from my view), they are not critical for learning programming (especially the fundamentals). I don't believe one would sacrifice anything by using free resouces, and may in fact gain something (like a fervant passion for freedom!). "Sometimes...things that are expensive...are worse."

### Why *Another* List of Programming Resources?!

I...(\*sniff\*)...just wanted to...(\*sniff\*)...make my own!

And also, I believe the costs of yet another resource list (e.g. redundancy, small storage/server requirements) is mitigated by it's (very humble) benefits (e.g. a chance of introducing something new to someone!).

### (Fuzzy) Requirements In Rough Order of Importance

1. **time** to read and learn and try out new concepts. Peter Norvig lays out a [good argument][10-years] for giving yourself 10 years to learn programming. You can learn a lot in 24 hours, but giving yourself enough time to really dive into coding can be beneficial. That said, don't feel like you have to sign a 10-year contract in order to be a "good programmer". As with anything, it's a day-by-day effort, dependent only on your own personal interests! You can spend a couple of minutes learning some programming idea this week, and you'll be ahead of where you started. That's fantastic! Moreover, whether you're 1 years old, or 111 years old, there's no specific "right time" to learn coding. It's whenever you do that's the "right time".
2. **translations/accessability** to read and watch resources in your preferred language or in a required medium. Many, if not most, resources default to English, but should have some translations available. And many resources (unfortunately not all) will have at lease some accessability capacities.
3. **internet access** in order to retreive the videos, texts, and tutorials listed below. Your internet does not need to be blazingly fast, but just needs to have enough broadband to download or stream textbooks and standard videos.
4. **a computer** in order to read, watch, and experiment with the resources listed below. Your computer does not need to be a flashy new machine, but instead can be anything with even a modest amount of memory and CPU power. Much of introductory coding is not at all computationally intensive, so any computer released in the last couple decades should do the trick.

