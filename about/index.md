---
layout: my-default
title: about
---

# hello! i'm liam. <object type="image/svg+xml" data="/assets/svg/iconSmile5Optimized.svg"></object>
<!-- <img> -->
<p style="font-family: 'EB Garamond'; padding-left: 11%; font-size: 2em">A STUCK-UP, HALF-WITTED, SCRUFFY-LOOKING NERF HERDER.</p>


## about me:
I'm a sleepy human with a dark history of secrets and intrigue.

Have a good book or movie recommendation or just want to chat? I'm available at <a href="mailto:liam@liambeckman.com">liam@liambeckman.com</a>

Are you looking for a plucky CS student with a small propensity for segmentation faults? Here are some links of interest:

<div class="inline-block">
    <ul class="showoff">
        <li class="featured" onclick="location.href='https://liambeckman.com/pkgs/resume/resume-liam-beckman.pdf'">
            <object class="showoff" type="image/svg+xml" data="/assets/svg/octicons-5.0.1/lib/svg/file-pdf.svg"></object><p> Resume (PDF)</p>
        </li>
        <li class="featured" onclick="location.href='https://git.liambeckman.com/cgit/resume/tree/resume-liam-beckman.tex'">
            <object class="showoff" type="image/svg+xml" data="/assets/svg/tex.svg"></object><p> Resume (<span class="latex">L<sup>a</sup>T<sub>e</sub>X</span>)</p>
        </li>
        <li class="featured" onclick="location.href='https://github.com/lbeckman314/'">
            <object class="showoff" type="image/svg+xml" data="/assets/svg/github.svg"></object><p> My Github profile</p>
        </li>
        <li class="featured" onclick="location.href='https://gitlab.com/lbeckman314/'">
            <object class="showoff" type="image/svg+xml" data="/assets/svg/gitlab-2.svg"></object><p> My Gitlab profile</p>
        </li>
        <li class="featured" onclick="location.href='https://www.linkedin.com/in/liam-beckman-ab3183a6/'">
            <object class="showoff" type="image/svg+xml" data="/assets/svg/linkedin.svg"></object><p> My LinkedIn profile</p>
        </li>
    </ul>
</div>

---

## about the website:

This website stores and presents some various works and projects. It originally got up and running thanks to Jonathan McGlone's wonderfully helpful [guide](http://jmcglone.com/guides/github-pages/).
<br/>
<br/>
The site is built by [Jekyll](https://jekyllrb.com/), hosted on my Raspberry Pi (Raspbian on RPi3 Model B), and encrypted by [Let's Encrypt](https://letsencrypt.org/) and [Certbot](https://certbot.eff.org/).
<br/>
<br/>
The [website's source code](https://github.com/lbeckman314/website) is yours. Feel free to copy and paste, fork, clone, or anything you like. Be forwarn! This site is written in an ungodly mix of markdown, html, and inline css. To spin up your own site, simply follow [the jekyll quick-start quide](https://jekyllrb.com/docs/quickstart/) (adapted below):

<br/>

```shell
# install ruby
# for OS-specific instructions, check out https://www.ruby-lang.org/en/downloads/

# Install Jekyll and Bundler gems through RubyGems
gem install jekyll bundler

# install dependencies
bundle install

# Create a new Jekyll site at ./myblog
jekyll new myblog

# alternatively, clone an existing jekyll site
# git clone https://github.com/lbeckman314/website

# Change into your new directory
cd myblog

# Build the site on the preview server
bundle exec jekyll serve

# Now browse to http://localhost:4000

# then you can change the html/css files in your website directory (e.g. myblog/) to suit your tastes!
# some cool templates → https://html5up.net/

# hosting is a whole 'nother beast, but services like github pages and gitlab pages make free hosting relatively easier.
# self-hosting with apache and/or nginx is another cool possibility!

```
