---
layout: my-default
title: code
---

<script src="https://cdn.plyr.io/3.5.4/plyr.js"></script>
<script>
    const player = new Plyr('#player');
</script>
<link rel="stylesheet" href="https://cdn.plyr.io/3.5.4/plyr.css" />

<video src="/assets/week4-progress-report.webm" id="player" controls data-plyr-config='{ "title": "Example Title" }'></video>
