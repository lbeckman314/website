---
layout: my-default
title: favorites
---

<h1 class="favorites">Favorites</h1>
<br />

<div class = "slideshow-container">
    <div class = "mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/ssss.jpg"></div>
        <div class="text"><a href="https://www.sssscomic.com">Stand Still Stay Slient</a></div>
    </div>

    <div class = "mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/gustav.jpg"></div>
        <div class="text"><a href="https://www.instagram.com/wikstromnaturfoto/">Wikström Naturfoto</a></div>
    </div>

    <div class = "mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/the-bald-future.jpg"></div>
        <div class="text"><a href="https://www.instagram.com/wikstromnaturfoto/">Le Futur Sera Chauve / The Bald Future</a></div>
    </div>

    <div class = "mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/false-knees.png"></div>
        <div class="text"><a href="http://falseknees.com/">False Knees</a></div>
    </div>


    <div class = "mySlides fade">
        <div class="numbertext">2 / 3</div>
        <div class="black-fade"><img class="slide" src="/assets/favorites/anne-frank-the-diary.jpg"></div>
        <div class="text">The Diary of Anne Frank</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/jonathan-strange-mr-norrell.jpg"></div>
        <div class="text">Jonathan Strange & Mr. Norrell</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/moominland.jpg"></div>
        <div class="text">Comet in Moominland</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/contact.jpg"></div>
        <div class="text">Contact</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/downward-bound.jpg"></div>
        <div class="text">Downward Bound: A Mad! Guide to Rock Climbing</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/the-cartoon-history-of-time.jpg"></div>
        <div class="text">The Cartoon History of Time</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/to-kill-a-mockingbird.jpg"></div>
        <div class="text">To Kill a Mockingbird</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/sisters-brothers.jpg"></div>
        <div class="text">The Sisters Brothers</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/american-graffiti.jpg"></div>
        <div class="text">American Graffiti</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/before-sunrise.jpg"></div>
        <div class="text">Before Sunrise Trilogy</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/moonlight.jpg"></div>
        <div class="text">Moonlight</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/raw.jpg"></div>
        <div class="text">Raw</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/saul-fia.jpg"></div>
        <div class="text">Son of Saul</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/memories-of-murder.jpg"></div>
        <div class="text">Memories of Murder</div>
    </div>

    <div class="mySlides fade">
        <div class="black-fade"><img class="slide" src="/assets/favorites/KHdesign.jpg"></div>
        <div class="text">Knifepoint Horror</div>
    </div>

    <a class = "prev-slideshow" onclick="plusSlides(-1)"></a>
    <a class = "next-slideshow" onclick="plusSlides(+1)"></a>

</div>


## artwork

※ [Stand Still Stay Slient](https://www.sssscomic.com) ⇢ a beautifully illustrated webcomic by Minna Sundberg.

※ [Wikström Photos](https://www.instagram.com/wikstromnaturfoto/) ⇢ simply amazing photos of nature and the world.

※ [Le Futur Sera Chauve / The Bald Future](https://vimeo.com/257716930) ⇢ lovely animated short by Paul Cabon.

※ [False Knees](http://falseknees.com/) ⇢ Comics about birds.

## books

※ [The Diary of Anne Frank](https://en.wikipedia.org/wiki/Anne_Frank) ⇢ "...when I look up at the sky, I somehow feel that this cruelty too shall end, and that peace & tranquility will return once again."

※ [Jonathan Strange & Mr Norrell](https://en.wikipedia.org/wiki/Jonathan_Strange_%26_Mr_Norrell) ⇢ A really cool book about magic and fancy English society.

※ [Comet In Moominland](https://en.wikipedia.org/wiki/Comet_in_Moominland) ⇢ What happens when a comet is bearing down on the entire world? Adventures of course!

※ [Downward Bound: A Mad! Guide to Rock Climbing](http://publications.americanalpineclub.org/articles/12197656700/Downward-Bound-A-Mad-Guide-to-Rock-Climbing) ⇢ How to rock climb and fall into disrepute.

※ [The Cartoon History of Time](http://www.katecharlesworth.com/time/index.html) ⇢ Ever been taught physics by a chicken and a cat?

※ [To Kill A Mockingbird](https://en.wikipedia.org/wiki/To_Kill_a_Mockingbird) ⇢ Timeless classic.

※ [The Sisters Brothers](https://en.wikipedia.org/wiki/The_Sisters_Brothers) ⇢ A tale of two brothers in the old west.

## movies and shows

※ [American Graffiti](https://en.wikipedia.org/wiki/American_Graffiti) ⇢ A single night in the 1960's.

※ [The Before Trilogy](https://en.wikipedia.org/wiki/Before_Sunrise) ⇢ Two people walking around, pondering love and life.

※ [Moonlight](https://en.wikipedia.org/wiki/Moonlight_(2016_film)) ⇢ A tender look into one man's life.

※ [Raw](https://en.wikipedia.org/wiki/Raw_(film)) ⇢ When cannibalistic tendencies go wrong.

※ [Son of Saul](https://en.wikipedia.org/wiki/Son_of_Saul) ⇢ A beautifully crafted film. Heartbreaking and harsh.

※ [Memories of Murder](https://en.wikipedia.org/wiki/Memories_of_Murder) ⇢ A fraught, and at times futile, search for a criminal.

## podcasts

※ [Knifepoint Horror](https://knifepointhorror.libsyn.com/) ⇢ The best horror stories around.

## music

※ [Spiegel im Spiegel](https://en.wikipedia.org/wiki/Spiegel_im_Spiegel) ⇢ Beautiful classical piece.

## poems

※ [Getting Used to Your Name](https://www.poetryfoundation.org/poems/52565/getting-used-to-your-name) By Marin Sorescu, Translated by Gabriela Dragnea

```
After you’ve learned to walk,
Tell one thing from another,
Your first care as a child
Is to get used to your name.
What is it?
They keep asking you.
You hesitate, stammer,
And when you start to give a fluent answer
Your name’s no longer a problem.

When you start to forget your name,
It’s very serious.
But don’t despair,
An interval will set in.

And soon after your death,
When the mist rises from your eyes,
And you begin to find your way
In the everlasting darkness,
Your first care (long forgotten,
Long since buried with you)
Is to get used to your name.
You’re called — just as arbitrarily —
Dandelion, cowslip, cornel,
Blackbird, chaffinch, turtle dove,
Costmary, zephyr — or all these together.
And when you nod, to show you’ve got it,
Everything’s all right:
The earth, almost round, may spin
Like a top among stars.
```

※ [Things](https://www.poetryfoundation.org/poems/53133/things-56d2322956d0a) By Lisel Mueller

```
What happened is, we grew lonely
living among the things,
so we gave the clock a face,
the chair a back,
the table four stout legs
which will never suffer fatigue.

We fitted our shoes with tongues
as smooth as our own
and hung tongues inside bells
so we could listen
to their emotional language,

and because we loved graceful profiles
the pitcher received a lip,
the bottle a long, slender neck.

Even what was beyond us
was recast in our image;
we gave the country a heart,
the storm an eye,
the cave a mouth
so we could pass into safety.
```

※ [Bleak Weather](https://www.poetryfoundation.org/poems/53212/bleak-weather) By Ella Wheeler Wilcox

```
Dear love, where the red lillies blossomed and grew,
The white snows are falling;
And all through the wood, where I wandered with you,
The loud winds are calling;
And the robin that piped to us tune upon tune,
Neath the elm—you remember,
Over tree-top and mountain has followed the June,
And left us—December.

Has left, like a friend that is true in the sun,
And false in the shadows.
He has found new delights, in the land where he's gone,
Greener woodlands and meadows.
What care we? let him go! let the snow shroud the lea,
Let it drift on the heather!
We can sing through it all; I have you—you have me,
And we’ll laugh at the weather.

The old year may die, and a new one be born
That is bleaker and colder;
But it cannot dismay us; we dare it—we scorn,
For love makes us bolder.
Ah Robin! sing loud on the far-distant lea,
Thou friend in fair weather;
But here is a song sung, that’s fuller of glee,
By two warm hearts together.

```

※ [The Sciences Sing a Lullabye](https://poets.org/poem/sciences-sing-lullabye) By Albert Goldbarth  

```
Physics says: go to sleep. Of course
you're tired. Every atom in you
has been dancing the shimmy in silver shoes
nonstop from mitosis to now.
Quit tapping your feet. They'll dance
inside themselves without you. Go to sleep.

Geology says: it will be all right. Slow inch
by inch America is giving itself
to the ocean. Go to sleep. Let darkness
lap at your sides. Give darkness an inch.
You aren't alone. All of the continents used to be
one body. You aren't alone. Go to sleep.

Astronomy says: the sun will rise tomorrow,
Zoology says: on rainbow-fish and lithe gazelle,
Psychology says: but first it has to be night, so
Biology says: the body-clocks are stopped all over town
and
History says: here are the blankets, layer on layer, down and down. 
```

※ [The Sea Shell](https://www.poetryfoundation.org/poems/51021/the-sea-shell) By Marin Sorescu, Translated by Michael Hamburger

```
I have hidden inside a sea shell
but forgotten in which.

Now daily I dive,
filtering the sea through my fingers,
to find myself.
Sometimes I think
a giant fish has swallowed me.
Looking for it everywhere I want to make sure
it will get me completely.

The sea-bed attracts me, and
I'm repelled by millions
of sea shells that all look alike.
Help, I am one of them.
If only I knew, which.

How often I've gone straight up
to one of them, saying: That’s me.
Only, when I prised it open
it was empty.
```


※ [Summer at North Farm](https://www.poetryfoundation.org/poetrymagazine/poems/37522/summer-at-north-farm) By Stephen Kuusisto

```
Summer at North Farm
By Stephen Kuusisto

Finnish rural life, ca. 1910

Fires, always fires after midnight,
the sun depending in the purple birches

and gleaming like a copper kettle.
By the solstice they’d burned everything,

the bad-luck sleigh, a twisted rocker,
things “possessed” and not-quite-right.

The bonfire coils and lurches,
big as a house, and then it settles.

The dancers come, dressed like rainbows
(if rainbows could be spun),

and linking hands they turn
to the melancholy fiddles.

A red bird spreads its wings now
and in the darker days to come.
```

※ [Flowers](https://www.poetryfoundation.org/poetrymagazine/poems/55300/flowers-56d236be6a805) By Wendy Videlock

```
/for my mother/

They are fleeting.
They are fragile.
They require

little water.
They'll surprise you.
They'll remind you

that they aren't
and they are you.
```


※ [Example of Recursion](https://stackoverflow.com/questions/126756/examples-of-recursive-functions/126785##126785) by wharfinger

```
A CS professor once explained recursion as follows:
A child couldn't sleep, so her mother told her a story about a little frog,
    who couldn't sleep, so the frog's mother told her a story about a little bear,
        who couldn't sleep, so the bear's mother told her a story about a little weasel...
            who fell asleep.
            ...and the little bear fell asleep;
        ...and the little frog fell asleep;
...and the child fell asleep.
```


※ [Point by Point](http://www.tahirihthepureone.com/PointByPoint.php) by Táhirih (Qurratu l-ʿAyn)

```
If I met you face to face, I
would retrace—erase!—my heartbreak,
    pain by pain,
    ache by ache,
    word by word,
    point by point.
In search of you—just your face!—I
roam through the streets lost in disgrace,
    house to house,
    lane to lane,
    place to place,
    door to door.
My heart hopeless—broken,crushed!—I
heard it pound, till blood gushed from me,
    fountain by fountain,
    stream by stream,
    river by river,
    sea by sea.
The garden of your lips—your cheeks!—
your perfumed hair, I wonder there,
    bloom to bloom,
    rose to rose,
    petal to petal,
    scent to scent.
Your eyebrow—your eye!—and the mole
on your face, somehow they tie me,
    trait to trait,
    kindness to kindness,
    passion to passion,
    love to love.
While I grieve, with love—your love!—I
will reweave the fabric of my soul,
    stitch by stitch,
    thread by thread,
    warp by warp,
    woof by woof.
Last, I—Tahirih—searched my heart, I
looked line by line. What did I find?
    You and you,
    you and you,
    you and you.
```

※ [Recursion](https://www.poetryfoundation.org/poetrymagazine/browse?contentId=39682) by Maria M. Benet

```
A tomato I overlooked on the window ledge,
remembers the hold of vine, the brace of ground,
and puts down roots inside its own flesh.

Halved under the blade of my knife,
the tomato unbinds its shoots, sends them
into an abyss of air and light—

Here at my desk I sit remembering,
putting down words far from the vine
of a native tongue, as if they could be roots,

each, like the tomato with its faith curlicues
in pale inward shoots, calling to itself,
back to the source of fruit.
```

※ [The Tea and Sage Poem](https://www.poetryfoundation.org/poems/54517/the-tea-and-sage-poem) by Fady Joudah

```
At a desk made of glass,
In a glass walled-room
With red airport carpet,

An officer asked
My father for fingerprints,
And my father refused,

So another offered him tea
And he sipped it. The teacup
Template for fingerprints.

My father says, it was just
Hot water with a bag.
My father says, in his country,

Because the earth knows
The scent of history,
It gave the people sage.

I like my tea with sage
From my mother’s garden,
Next to the snapdragons

She calls fishmouths
Coming out for air. A remedy
For stomach pains she keeps

In the kitchen where
She always sings.
First, she is Hagar

Boiling water
Where tea is loosened.
Then she drops

In it a pinch of sage
And lets it sit a while.
She tells a story:

The groom arrives late
To his wedding
Wearing only one shoe.

The bride asks him
About the shoe. He tells her
He lost it while jumping

Over a house-wall.
Breaking away from soldiers.
She asks:

Tea with sage
Or tea with mint?

With sage, he says,
Sweet scent, bitter tongue.
She makes it, he drinks.
```

# ※

<script src="/assets/js/slideshow.js"></script>
